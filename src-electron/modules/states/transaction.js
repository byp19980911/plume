import { ipcMain as ipc } from 'electron'
import { EventEmitter } from 'events'
import _ from 'lodash'
import logger from '../logger'
import { Types } from '../ipc/types'
// import fse from 'fs-extra'
// import settings from '../settings'

const log = logger.create('TransactionState')

class TransactionState extends EventEmitter {
  constructor () {
    super()
    this.on('sync', this._sync)

    ipc.on(Types.SEND_TRANSACTION, _.bind(this._sendTransaction, this))
  }

  _sync () {
    log.info('load transactions from db')
    const db = global.db
    let transactions = db.transactions
      .chain()
      .simplesort('timestamp', true)
      .data()
    global.windows.broadcast(Types.SYNC_TRANSACTION, { transactions })
  }

  _sendTransaction (event, obj) {
    // const web3 = global.web3
    // log.info(
    //   'ipc call send transaction: \n',
    //   _.pick(obj.tx, ['from', 'to', 'value', 'data'])
    // )
    // let keyPath = obj.keyPath
    // let kfile = keyPath
    // let privateKey = ''
    // try {
    //   let decryptObj = web3.eth.accounts.decrypt(JSON.stringify(kfile), obj.password)
    //   privateKey = decryptObj.privateKey
    //   privateKey = Buffer.from(privateKey.substring(2), 'hex')
    // } catch (e) {
    //   if (e.message === 'Key derivation failed - possibly wrong password') {
    //     let reply = { error: 'invalid-password' }
    //     event.sender.send(Types.SEND_TRANSACTION_REPLY, reply)
    //     return
    //   }
    // }
    // let chainId = settings.chainId
    // chainId = web3.utils.toHex(chainId)
    // let tx = obj.tx
    //
    // let rawTx = {
    //   'from': tx.from,
    //   'gasPrice': web3.utils.toHex(tx.gasPrice),
    //   'gasLimit': web3.utils.toHex(tx.gas),
    //   'to': tx.to,
    //   'data': '',
    //   'value': web3.utils.toHex(tx.value),
    //   'chainId': web3.utils.toHex(chainId)
    // }
    //
    // web3.eth.getTransactionCount(tx.from, 'pending').then(transactionCount => {
    //   rawTx['nonce'] = '0x' + transactionCount.toString(16)
    //   log.info(rawTx)
    //   const EthTx = require('ethereumjs-tx')
    //   let tr = new EthTx(rawTx)
    //   tr.sign(privateKey)
    //   let serializedTx = tr.serialize()
    //   return web3.eth
    //     .sendSignedTransaction('0x' + serializedTx.toString('hex'))
    //     .once('transactionHash', hash => {
    //       let reply = { transactionHash: hash, tx }
    //       event.sender.send(Types.SEND_TRANSACTION_REPLY, reply)
    //     })
    //     .on('error', error => {
    //       throw error
    //     })
    // })
    // let tx = obj.tx
    // // let _this = this
    // web3.eth.personal
    //   .unlockAccount(tx.from, obj.password)
    //   .then(result => {
    //     return web3.eth
    //       .sendTransaction(tx)
    //       .once('transactionHash', hash => {
    //         let reply = { transactionHash: hash, tx }
    //         event.sender.send(Types.SEND_TRANSACTION_REPLY, reply)
    //       })
    //       .on('error', error => {
    //         throw error
    //       })
    //   })
    //   .catch(error => {
    //     log.error(error)
    //     let reply = { error: 'invalid-password' }
    //     event.sender.send(Types.SEND_TRANSACTION_REPLY, reply)
    //   })
    //   .finally(() => {
    //     web3.eth.personal.lockAccount(tx.from)
    //   })
  }
}

export default new TransactionState()
